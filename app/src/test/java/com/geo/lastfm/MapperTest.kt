package com.geo.lastfm

import com.geo.lastfm.geo.data.mapper.ArtistEntityMapper
import com.geo.lastfm.geo.data.model.response.Artist
import com.geo.lastfm.geo.data.model.response.Image
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class MapperTest {
    private lateinit var artistEntityMapper: ArtistEntityMapper

    @Before
    fun setUp() {
        artistEntityMapper = ArtistEntityMapper()
    }

    @Test
    fun `Transform Artist into ArtistEntity`() {
        val artist = Artist("NAME1", 100, "ID1", listOf(Image("http://img1.com" , "small"), Image("http://img2.com" , "medium"), Image("http://img3.com" , "large")))
        val artistEntity = artistEntityMapper.transform(artist)
        assertEquals(artistEntity.name, artist.name)
        assertEquals(artistEntity.listeners, artist.listeners)
        assertEquals(artistEntity.thumbnail, "http://img1.com")
        assertEquals(artistEntity.image, "http://img2.com")
    }

    @Test
    fun `Transform Artist list into Artist entity list`() {
        val artistList = listOf(
            Artist("NAME1", 100, "ID1", listOf(Image("http://img1.com" , "small"), Image("http://img1.com" , "medium"), Image("http://img1.com" , "large"))),
            Artist("NAME2", 200, "ID2", listOf(Image("http://img2.com" , "small"), Image("http://img1.com" , "medium"), Image("http://img1.com" , "large")))
        )

        val artistEntities = artistEntityMapper.transformList(artistList)
        assertEquals(artistList.size, artistEntities.size)
        assertEquals(artistList.first().mbid, artistEntities.first().id)
        assertNotNull(artistEntities.first().thumbnail)
        assertNotNull(artistEntities.first().image)
    }
}