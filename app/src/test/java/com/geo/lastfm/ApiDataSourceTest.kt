package com.geo.lastfm

import com.geo.lastfm.base.exception.HttpCallFailureException
import com.geo.lastfm.geo.data.mapper.ArtistEntityMapper
import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import com.geo.lastfm.geo.data.model.response.Artist
import com.geo.lastfm.geo.data.model.response.ArtistApiResponse
import com.geo.lastfm.geo.data.model.response.Image
import com.geo.lastfm.geo.data.model.response.TopArtists
import com.geo.lastfm.geo.data.net.GeoService
import com.geo.lastfm.geo.data.repository.datasource.GeoApiDataSource
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import retrofit2.HttpException

class ApiDataSourceTest {

    lateinit var compositeDisposable: CompositeDisposable
    lateinit var geoApiDataSource: GeoApiDataSource
    lateinit var geoService: GeoService

    @Before
    fun setUp() {
        geoService = mock()
        geoApiDataSource = GeoApiDataSource(geoService, ArtistEntityMapper())
        compositeDisposable = CompositeDisposable()

    }

    @Test
    fun `when error is throwed error type should be the expected`() {

        val exception : HttpException = HttpException(mock())
        `when`(geoService.getTopArtists(any(), any())).thenReturn(Flowable.error(exception))

        geoApiDataSource.getTopArtists("spain", 1)
            .test()
            .assertError{
                it is HttpCallFailureException
            }
    }

    @Test
    fun `when artist list api call executed then a correct artist entity list should be provided`() {
        val artistList = listOf(
            Artist("NAME1", 100, "ID1", listOf(Image("http://img1.com" , "small"), Image("http://img2.com" , "medium"), Image("http://img1.com" , "large"))),
            Artist("NAME2", 200, "ID2", listOf(Image("http://img2.com" , "small"), Image("http://img1.com" , "medium"), Image("http://img1.com" , "large")))
        )
        val artistResponse = ArtistApiResponse(TopArtists(artistList))

        val artists = listOf(
            ArtistEntity("ID1", "NAME1", 100, "http://img1.com", "http://img2.com")
        )

        val obs = Flowable.just(artistResponse)
        whenever(geoService.getTopArtists(any(), any())).thenReturn(obs)

        geoApiDataSource.getTopArtists("spain", 1)
            .test()
            .assertValue {
                it.first() == artists.first() }

    }
}