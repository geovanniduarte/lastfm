package com.geo.lastfm

import android.content.Intent
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers

import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.geo.lastfm.geo.ui.list.GeoListActivity

import com.geo.lastfm.geo.ui.list.artist.adapter.ArtistViewHolder
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ActivityTest {

    val activityTestRule = ActivityTestRule<GeoListActivity>(GeoListActivity::class.java, false, false)

    @Test
    fun testBasicInitialization() {
        activityTestRule.launchActivity(Intent())
        Espresso.onView(ViewMatchers.withText(R.string.app_name)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun testFirstRecyclerViewItemShowsCorrectText() {

        activityTestRule.launchActivity(Intent())
        onView(withId(R.id.item_list))
            .check(matches(
                hasDescendant(withText("David Bowie"))
            ))

    }
    

    @Test
    fun testClickOnItemAndOpenDetailFragment() {
        activityTestRule.launchActivity(Intent())

        onView(withId(R.id.item_list))
            .perform(RecyclerViewActions.actionOnItemAtPosition<ArtistViewHolder>(0, click()))

        onView(withId(R.id.image_detail))
            .check(matches(isDisplayed()))

    }
}