package com.geo.lastfm.base

class Constants {
    companion object {
        const val DEFAULT_SITE = "spain"
        const val FM_PREFS = "FMPREFS"
        const val SEARCH_DEBOUNCE_TIME = 1L
        const val IMAGE_DETAIL_TRANSITION_NAME = "sharedImage"
        const val PAGE_SIZE = 50
    }
}