package com.geo.lastfm.base.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.geo.lastfm.R
import com.geo.lastfm.base.exception.NetworkException
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import javax.inject.Named

abstract class BaseViewModel : ViewModel(),
    MVVMViewModel {
    @Inject
    @Named("isLoadingState")
    lateinit var isLoadingState: MutableLiveData<Boolean>

    @Inject
    @Named("errorMessageState")
    lateinit var errorMessageState : MutableLiveData<String>

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var context: Context

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    override fun handleApiError(error: Throwable?) {

        if (error != null && error is NetworkException) {
            errorMessageState.value = error.getCustomMessage(context)
        } else {
            errorMessageState.value = context.getString(R.string.error_unknown)
        }
    }

    override fun setUserAsLoggedOut() {
        TODO("Not yet implemented")
    }

}