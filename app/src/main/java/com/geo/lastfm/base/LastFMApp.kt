package com.geo.lastfm.base

import android.app.Application
import androidx.multidex.MultiDexApplication
import com.geo.lastfm.geo.di.module.ApplicationModule
import com.geo.lastfm.geo.di.component.ApplicationComponent
import com.geo.lastfm.geo.di.component.DaggerApplicationComponent

class LastFMApp : MultiDexApplication() {

    var applicationComponent: ApplicationComponent? = null

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }
}