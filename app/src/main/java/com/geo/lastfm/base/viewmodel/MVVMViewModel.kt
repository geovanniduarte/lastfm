package com.geo.lastfm.base.viewmodel

interface MVVMViewModel {

    fun handleApiError(error: Throwable?)

    fun setUserAsLoggedOut()
}