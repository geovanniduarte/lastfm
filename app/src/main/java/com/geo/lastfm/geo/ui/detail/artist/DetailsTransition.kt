package com.geo.lastfm.geo.ui.detail.artist

import androidx.transition.ChangeBounds
import androidx.transition.ChangeImageTransform
import androidx.transition.ChangeTransform
import androidx.transition.TransitionSet

public class DetailsTransition: TransitionSet {
    constructor() {
        ordering = ORDERING_TOGETHER
        addTransition( ChangeBounds())
            .addTransition( ChangeTransform())
            .addTransition( ChangeImageTransform())
    }
}