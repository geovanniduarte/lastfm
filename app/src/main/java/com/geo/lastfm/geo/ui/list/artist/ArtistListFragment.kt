package com.geo.lastfm.geo.ui.list.artist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagedList
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.geo.lastfm.R
import com.geo.lastfm.base.ui.BaseFragment
import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import com.geo.lastfm.geo.di.component.GeoListComponent
import com.geo.lastfm.geo.ui.list.GeoListViewModel
import com.geo.lastfm.geo.ui.list.artist.adapter.ArtistAdapter
import com.geo.lastfm.geo.util.Navigator
import kotlinx.android.synthetic.main.fragment_item_list.*
import javax.inject.Inject

private const val ARG_QUERY = "query"

class ArtistListFragment : BaseFragment() {

    private var query: String = ""

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var navigator: Navigator

    lateinit var viewModel: GeoListViewModel

    private val adapter =
        ArtistAdapter { artist, image ->
            onArtistClicked(artist, image)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        arguments?.let {
            query = it.getString(ARG_QUERY, "")
        }
    }

    private fun inject() {
        activityComponent?.inject(this)
    }

    override fun setUp(view: View?) {
        setUpViewModel()
        setUpArtistList()
        setUpView()
        bindEvents()
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(this.activity!!, viewModelFactory).get(GeoListViewModel::class.java)
    }

    private fun setUpArtistList() {
        item_list.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        item_list.itemAnimator = DefaultItemAnimator()
        item_list.adapter = adapter
    }

    private fun setUpView() {
        button_reload.setOnClickListener {
            resetView()
        }
    }

    private fun resetView() {
        button_reload.visibility = View.GONE
        message.text = ""
    }

    private fun bindEvents() {
        viewModel.artistList.observe(this, Observer {
            populateArtistList(it)
        })

        viewModel.errorMessageState.observe(this, Observer { message ->
            onError(message)
        })
    }

    override fun onError(message: String?) {
        showMessage(message)
        button_reload.visibility = View.VISIBLE
    }

    override fun showMessage(messageStr: String?) {
        message.text = messageStr
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)
        val component: GeoListComponent? = activityComponent
        component?.let {
            component.inject(this)
        }
        return view
    }

    private fun populateArtistList(artists: PagedList<ArtistEntity>) {
        resetView()
        adapter.submitList(artists)
    }

    private fun onArtistClicked(artistEntity: ArtistEntity, imageView: View) {
        navigator.navigatesToDetail(this.activity!!.supportFragmentManager, artistEntity, imageView)
    }

    fun updateQuery(query: String) {
        this.query = query
        resetView()
    }

    companion object {
        @JvmStatic
        fun newInstance(query: String) =
            ArtistListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_QUERY, query)
                }
            }
    }
}