package com.geo.lastfm.geo.ui.list.artist.adapter

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.geo.lastfm.geo.data.model.entity.ArtistEntity

class ArtistAdapter(val onClickListener: Click): PagedListAdapter<ArtistEntity, RecyclerView.ViewHolder>(ArtistDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ArtistViewHolder.create(parent, onClickListener)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ArtistViewHolder) {
            holder.bind(getItem(position))
        }
    }

    companion object {
        val ArtistDiffCallback = object : DiffUtil.ItemCallback<ArtistEntity>() {
            override fun areItemsTheSame(oldItem: ArtistEntity, newItem: ArtistEntity): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: ArtistEntity, newItem: ArtistEntity): Boolean {
                return oldItem == newItem
            }
        }
    }
}