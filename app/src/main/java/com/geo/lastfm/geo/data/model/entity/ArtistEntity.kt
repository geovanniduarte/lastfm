package com.geo.lastfm.geo.data.model.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ArtistEntity(
    val id: String,
    val name: String,
    val listeners: Long,
    val thumbnail: String?,
    val image: String?
) : Parcelable