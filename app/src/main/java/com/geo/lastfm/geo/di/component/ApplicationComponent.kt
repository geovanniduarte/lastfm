package com.geo.lastfm.geo.di.component

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.geo.lastfm.geo.data.repository.datasource.ArtistDataSourceFactory
import com.geo.lastfm.geo.data.repository.datasource.GeoApiDataSource
import com.geo.lastfm.geo.di.module.ApplicationModule
import com.geo.lastfm.geo.di.module.DataModule
import com.geo.lastfm.geo.di.module.NetModule
import com.geo.lastfm.geo.di.module.ViewModelModule
import com.geo.lastfm.geo.util.Navigator
import com.geo.lastfm.geo.util.SiteManager
import dagger.Component
import io.reactivex.Scheduler
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetModule::class, DataModule::class])
interface ApplicationComponent {
    fun getContext(): Context
    fun getNavigator(): Navigator
    fun getGeoApiDataSource(): GeoApiDataSource
    @Named("mainScheduler")
    fun getMainScheduler(): Scheduler
    @Named("ioScheduler")
    fun getIOScheduler(): Scheduler
    fun getSiteManager(): SiteManager
}