package com.geo.lastfm.geo.di.module

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.geo.lastfm.base.Constants.Companion.PAGE_SIZE
import com.geo.lastfm.geo.data.repository.datasource.ArtistDataSourceFactory
import com.geo.lastfm.geo.data.repository.datasource.GeoApiDataSource
import com.geo.lastfm.geo.util.SiteManager
import com.geo.mercadolibre.base.di.ActivityContext
import com.geo.mercadolibre.base.di.PerActivity
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Named

@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    @ActivityContext
    @PerActivity
    fun provideActivityContext(): Activity = activity

    @Provides
    @PerActivity
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()


    @Provides
    @PerActivity
    fun provideArtistDataSourceFactory(geoApiDataSource: GeoApiDataSource,
                                       compositeDisposable: CompositeDisposable,
                                       siteManager: SiteManager,
                                       @Named("isLoadingState")
                                       isLoadingState: MutableLiveData<Boolean>,
                                       @Named("errorMessageState")
                                       errorLoadingState: MutableLiveData<String>,
                                       context: Context
    ): ArtistDataSourceFactory
            = ArtistDataSourceFactory(geoApiDataSource, compositeDisposable, siteManager,
        isLoadingState, errorLoadingState, context)

    @Provides
    @PerActivity
    fun providePageConfig(): PagedList.Config = PagedList.Config.Builder()
        .setPageSize(PAGE_SIZE)
        .setEnablePlaceholders(false)
        .build()

    @Provides
    @PerActivity
    @Named("isLoadingState")
    fun providesIsLoadingState(): MutableLiveData<Boolean> = MutableLiveData()

    @Provides
    @PerActivity
    @Named("errorMessageState")
    fun providesErrorMessageState(): MutableLiveData<String> = MutableLiveData()

}