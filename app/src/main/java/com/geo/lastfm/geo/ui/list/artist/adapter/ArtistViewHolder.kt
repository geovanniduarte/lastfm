package com.geo.lastfm.geo.ui.list.artist.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.geo.lastfm.R
import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import kotlinx.android.synthetic.main.dialog_wait.view.*
import kotlinx.android.synthetic.main.item.view.*
typealias Click = (ArtistEntity, View) -> Unit

class ArtistViewHolder(view: View, val onClickListener: Click): RecyclerView.ViewHolder(view) {

    fun bind(artistEntity: ArtistEntity?) = with(itemView) {
        artistEntity?.let { artist ->
            Glide.with(itemView.context)
                .asBitmap()
                .load(artist.thumbnail)
                .into(item_image)
            txv_name.text = artist.name
            txv_listeners.text = artistEntity.listeners.toString()
            setOnClickListener { onClickListener(artist, item_image) }
            ViewCompat.setTransitionName(item_image, artist.id)
        }
    }

    companion object {
        fun create(parent: ViewGroup, callback: Click): ArtistViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.item, parent, false)
            return ArtistViewHolder(view, callback)
        }
    }
}