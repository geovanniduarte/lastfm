package com.geo.lastfm.geo.di.module

import com.geo.lastfm.BuildConfig
import com.geo.lastfm.geo.data.mapper.ArtistEntityMapper
import com.geo.lastfm.geo.data.net.GeoService
import com.geo.lastfm.geo.data.repository.datasource.GeoApiDataSource
import dagger.Module
import dagger.Provides
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
class NetModule {

    @Provides
    @Singleton
    fun provideRetrofitClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(loggingInterceptor)
        httpClient.addInterceptor(object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val original: Request = chain.request()
                val originalHttpUrl: HttpUrl = original.url
                val url = originalHttpUrl.newBuilder()
                    .addQueryParameter("api_key", "829751643419a7128b7ada50de590067")
                    .build()

                // Request customization: add request headers
                val requestBuilder: Request.Builder = original.newBuilder()
                    .url(url)
                val request: Request = requestBuilder.build()
                return chain.proceed(request)
            }
        })
        return httpClient.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(httpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
            .client(httpClient)
            .baseUrl(BuildConfig.API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    }

    @Provides
    @Singleton
    fun provideGeoService(retrofit: Retrofit) : GeoService = retrofit.create(GeoService::class.java)

    @Provides
    @Singleton
    fun provideGeoApiDataSource(geoService: GeoService, artistEntityMapper: ArtistEntityMapper)
            : GeoApiDataSource = GeoApiDataSource(geoService, artistEntityMapper)
}