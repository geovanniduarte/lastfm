package com.geo.lastfm.geo.data.net

import com.geo.lastfm.geo.data.model.response.ArtistApiResponse
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface GeoService {

    @GET(value = "2.0/?method=geo.gettopartists&format=json")
    fun getTopArtists(@Query("country") country: String, @Query("page") page: Int): Flowable<ArtistApiResponse>

    @GET(value = "2.0/?method=geo.gettoptracks&format=json")
    fun getTopTracks(@Query("country") country: String): Flowable<ArtistApiResponse>

}