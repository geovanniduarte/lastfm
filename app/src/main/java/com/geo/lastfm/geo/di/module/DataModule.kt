package com.geo.lastfm.geo.di.module

import android.content.Context
import android.content.SharedPreferences
import com.geo.lastfm.base.Constants
import com.geo.lastfm.geo.data.mapper.ArtistEntityMapper
import com.geo.lastfm.geo.data.repository.datasource.ArtistDataSourceFactory
import com.geo.lastfm.geo.data.repository.datasource.GeoApiDataSource
import com.geo.lastfm.geo.util.SiteManager
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    fun provideArtistEntityMapper(): ArtistEntityMapper = ArtistEntityMapper()

    @Singleton
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences = context.getSharedPreferences(
        Constants.FM_PREFS, Context.MODE_PRIVATE)

    @Singleton
    @Provides
    fun provideSiteManager(preferences: SharedPreferences) : SiteManager = SiteManager(preferences)
}