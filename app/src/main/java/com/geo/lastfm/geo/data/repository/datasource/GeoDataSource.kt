package com.geo.lastfm.geo.data.repository.datasource

import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import io.reactivex.Flowable

interface GeoDataSource {
    fun getTopArtists(country: String, page: Int): Flowable<List<ArtistEntity>>
}