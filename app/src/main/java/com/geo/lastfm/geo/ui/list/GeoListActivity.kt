package com.geo.lastfm.geo.ui.list

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.geo.lastfm.R
import com.geo.lastfm.base.ui.BaseActivity
import com.geo.lastfm.geo.util.Navigator
import kotlinx.android.synthetic.main.activity_geo_list.*
import javax.inject.Inject

class GeoListActivity: BaseActivity(),
SearchView.OnQueryTextListener,
SearchView.OnCloseListener {

    private lateinit var searchView: SearchView

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var navigator: Navigator

    private lateinit var viewModel: GeoListViewModel

    private var query: String = ""

    override fun onSaveInstanceState(outState: Bundle) {
        outState?.run {
            putString(QUERY, query)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.run {
            query = getString(QUERY, "")
        }
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        navigator.installArtistListFragment(supportFragmentManager)
        setContentView(R.layout.activity_geo_list)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setUpViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_item_list, menu)
        searchView = menu?.findItem(R.id.menu_search)?.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        searchView.setOnCloseListener(this)
        searchView.setQuery(query, true)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when(item.itemId) {
        android.R.id.home -> {
            supportFragmentManager.popBackStack()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun inject() {
        mUIComponent?.inject(this)
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(this, viewModelFactory).get(GeoListViewModel::class.java)
        bindEvents()
    }

    private fun bindEvents() {
        viewModel.isLoadingState.observe(this, Observer { isLoading ->
            if (isLoading) showLoading() else hideLoading()
        })
    }

    override fun showLoading() {
        list_progress_bar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        list_progress_bar.visibility = View.GONE
    }

    //OnQueryTextListener
    override fun onQueryTextSubmit(query: String?): Boolean {
        query?.let {

        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        newText?.let {
            query = newText
        }
        return true
    }

    //OnCloseListener
    override fun onClose(): Boolean {
        query = ""
        return false
    }

    companion object {
        val QUERY = "query_string"
    }
}