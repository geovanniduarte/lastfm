package com.geo.lastfm.geo.util

import android.os.Build
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.transition.Fade
import com.geo.lastfm.R
import com.geo.lastfm.base.Constants.Companion.IMAGE_DETAIL_TRANSITION_NAME
import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import com.geo.lastfm.geo.ui.detail.artist.DetailsTransition
import com.geo.lastfm.geo.ui.list.artist.ArtistDetailFragment
import com.geo.lastfm.geo.ui.list.artist.ArtistListFragment

class Navigator {

    fun installArtistListFragment(supportFragmentManager: FragmentManager) {
        val artistListFragment = ArtistListFragment.newInstance("")
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.root_fragment, artistListFragment, "artistslist")
            .addToBackStack(null)
            .commit()
    }

    fun navigatesToDetail(supportFragmentManager: FragmentManager, productEntity: ArtistEntity, imageView: View) {
        val details: ArtistDetailFragment = ArtistDetailFragment.newInstance(productEntity)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            details.sharedElementEnterTransition = DetailsTransition()
            details.enterTransition = Fade()
            details.exitTransition = Fade()
            details.sharedElementReturnTransition = DetailsTransition()
        }

        supportFragmentManager
            .beginTransaction()
            .addSharedElement(imageView, IMAGE_DETAIL_TRANSITION_NAME)
            .replace(R.id.root_fragment, details)
            .addToBackStack(null)
            .commit()
    }
}