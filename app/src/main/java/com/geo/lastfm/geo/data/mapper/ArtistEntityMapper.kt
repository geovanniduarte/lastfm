package com.geo.lastfm.geo.data.mapper

import com.geo.lastfm.base.mapper.Mapper
import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import com.geo.lastfm.geo.data.model.response.Artist
import com.geo.lastfm.geo.data.model.response.Image

enum class ImageSize {
    SMALL, MEDIUM, EXTRALARGE, MEGA
}

class ArtistEntityMapper: Mapper<Artist, ArtistEntity> {
    override fun transform(input: Artist): ArtistEntity = ArtistEntity(input.mbid, input.name, input.listeners, getImageFromSize(ImageSize.SMALL, input.image)?.text, getImageFromSize(ImageSize.MEDIUM, input.image)?.text)

    override fun transformList(inputList: List<Artist>): List<ArtistEntity> = inputList.map { transform(it) }

    private fun getImageFromSize(imageSize: ImageSize, images: List<Image>) = images.find { it.size == imageSize.name.toLowerCase() }
}