package com.geo.lastfm.geo.data.model.response

import com.google.gson.annotations.SerializedName

class Artist(
    val name: String,
    val listeners: Long,
    val mbid: String,
    val image: List<Image>

)

class Image(
    @SerializedName("#text")
    val text: String,
    val size: String
)