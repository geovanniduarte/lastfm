package com.geo.lastfm.geo.data.repository.datasource

import com.geo.lastfm.base.rx.mapNetworkErrors
import com.geo.lastfm.geo.data.mapper.ArtistEntityMapper
import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import com.geo.lastfm.geo.data.net.GeoService
import io.reactivex.Flowable

class GeoApiDataSource(private val geoService: GeoService, private val artistEntityMapper: ArtistEntityMapper): GeoDataSource {
    override fun getTopArtists(country: String, page: Int): Flowable<List<ArtistEntity>> =
        geoService.getTopArtists(country, page)
            .map { artistEntityMapper.transformList(it.topartists.artist) }
            .mapNetworkErrors()

}