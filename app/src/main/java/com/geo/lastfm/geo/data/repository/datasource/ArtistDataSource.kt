package com.geo.lastfm.geo.data.repository.datasource

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.geo.lastfm.R
import com.geo.lastfm.base.exception.NetworkException
import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import com.geo.lastfm.geo.util.SiteManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Named


class ArtistDataSource(private val geoApiDataSource: GeoApiDataSource,
                       private val compositeDisposable: CompositeDisposable,
                       private val siteManager: SiteManager,
                       private val isLoadingState: MutableLiveData<Boolean>,
                       private val errorMessageState : MutableLiveData<String>,
                       private val context: Context)
    : PageKeyedDataSource<Int, ArtistEntity>() {


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, ArtistEntity>
    ) {
        val currentPage = 1
        val nextPage = currentPage + 1
        isLoadingState.postValue(true)
        geoApiDataSource.getTopArtists(siteManager.currentSite, currentPage)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onNext = {
                    callback.onResult(it, null, nextPage)
                    isLoadingState.postValue(false)
                },
                onError = {
                    isLoadingState.postValue(false)
                    handleApiError(it)
                }
            )
            .addTo(compositeDisposable)
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, ArtistEntity>
    ) {
        val currentPage = params.key
        val nextPage = currentPage + 1
        isLoadingState.postValue(true)
        geoApiDataSource.getTopArtists(siteManager.currentSite, currentPage)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onNext = {
                    callback.onResult(it, nextPage)
                    isLoadingState.postValue(false)
                },
                onError = {
                    isLoadingState.postValue(false)
                    handleApiError(it)
                }
            )
            .addTo(compositeDisposable)

    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, ArtistEntity>
    ) {
        TODO("Not yet implemented")
    }

    private fun handleApiError(error: Throwable?) {
        if (error != null && error is NetworkException) {
            errorMessageState.value = error.getCustomMessage(context)
        } else {
            errorMessageState.value = context.getString(R.string.error_unknown)
        }
    }

}