package com.geo.lastfm.geo.di.component

import com.geo.lastfm.geo.di.module.ActivityModule
import com.geo.lastfm.geo.di.module.ViewModelModule
import com.geo.lastfm.geo.ui.list.GeoListActivity
import com.geo.lastfm.geo.ui.list.artist.ArtistListFragment
import com.geo.mercadolibre.base.di.PerActivity
import dagger.Component

@PerActivity
@Component(modules = [ActivityModule::class, ViewModelModule::class], dependencies = [ApplicationComponent::class])
interface GeoListComponent {
    fun inject(artistListFragment: ArtistListFragment)
    fun inject(geoListActivity: GeoListActivity)
}