package com.geo.lastfm.geo.data.repository.datasource

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import com.geo.lastfm.geo.util.SiteManager
import io.reactivex.disposables.CompositeDisposable


class ArtistDataSourceFactory(private val geoApiDataSource: GeoApiDataSource,
                              private val compositeDisposable: CompositeDisposable,
                              private val siteManager: SiteManager,
                              private val isLoadingState: MutableLiveData<Boolean>,
                              private val errorMessageState : MutableLiveData<String>,
                              private val context: Context)
    : DataSource.Factory<Int, ArtistEntity>() {

    val artistDataSourceLiveData = MutableLiveData<ArtistDataSource>()

    override fun create(): DataSource<Int, ArtistEntity> {
        val artistDataSource = ArtistDataSource(geoApiDataSource, compositeDisposable, siteManager, isLoadingState, errorMessageState, context)
        artistDataSourceLiveData.postValue(artistDataSource)
        return artistDataSource
    }
}