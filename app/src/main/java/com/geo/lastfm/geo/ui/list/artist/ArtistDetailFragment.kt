package com.geo.lastfm.geo.ui.list.artist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import com.bumptech.glide.Glide

import com.geo.lastfm.R
import com.geo.lastfm.base.Constants.Companion.IMAGE_DETAIL_TRANSITION_NAME
import com.geo.lastfm.base.ui.BaseFragment
import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import kotlinx.android.synthetic.main.fragment_artist_detail.*

private const val ARG_ARTIST = "artist"

class ArtistDetailFragment : BaseFragment() {

    companion object {
        fun newInstance(artist: ArtistEntity) = ArtistDetailFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_ARTIST, artist)
            }
        }
    }

    private var artist: ArtistEntity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_artist_detail, container, false)
        arguments?.let {
            artist = it.getParcelable(ARG_ARTIST)
        }
        return view
    }

    override fun setUp(view: View?) {
        ViewCompat.setTransitionName(image_detail, IMAGE_DETAIL_TRANSITION_NAME)
        hideKeyboard()
        artist?.let {
            syncView(it)
        }
    }


    private fun syncView(artistEntity: ArtistEntity) {
        Glide.with(this.context!!)
            .asBitmap()
            .load(artistEntity.image)
            .into(image_detail)

        txt_detail_listeners.text = "${artistEntity.listeners} ${getString(R.string.artist_entity_listeners)}"
        txt_detail_name.text = artistEntity.name
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
    }

}
