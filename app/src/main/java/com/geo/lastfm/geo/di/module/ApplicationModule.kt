package com.geo.lastfm.geo.di.module

import android.app.Application
import android.content.Context
import com.geo.lastfm.base.LastFMApp
import com.geo.lastfm.geo.util.Navigator
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: LastFMApp) {
    @Provides
    @Singleton
    fun provideApp(): Application = app

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideNavigator() = Navigator()

    @Provides
    @Singleton
    @Named("ioScheduler")
    fun providesIOScheduler(): Scheduler = Schedulers.io()

    @Provides
    @Singleton
    @Named("mainScheduler")
    fun providesMainScheduler(): Scheduler = AndroidSchedulers.mainThread()
}