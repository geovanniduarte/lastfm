package com.geo.lastfm.geo.data.model.response

class ArtistApiResponse(val topartists: TopArtists)

class TopArtists(val artist: List<Artist>)