package com.geo.lastfm.geo.ui.list

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.geo.lastfm.base.viewmodel.BaseViewModel
import com.geo.lastfm.geo.data.model.entity.ArtistEntity
import com.geo.lastfm.geo.data.repository.datasource.ArtistDataSourceFactory
import javax.inject.Inject

class GeoListViewModel @Inject constructor(private val artistDataSourceFactory: ArtistDataSourceFactory,
                                           private val config: PagedList.Config): BaseViewModel() {

    var artistList: LiveData<PagedList<ArtistEntity>> =
        LivePagedListBuilder(artistDataSourceFactory, config).build()

}