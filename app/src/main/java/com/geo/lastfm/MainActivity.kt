package com.geo.lastfm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.geo.lastfm.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
